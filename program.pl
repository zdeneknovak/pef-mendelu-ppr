#!/usr/bin/perl
=zadani
parametr bude webova adresa produktu na amazonu, najit odkaz na vsechny
uzivatelske review, stahnout vsecny prispevky a vytvorit soubor obsahujici
[pocet hvezdicek] [text] \n
=cut
use strict;
use LWP::Simple;
use Getopt::Long;
use Time::HiRes qw(time);

my $start = time;
my $data_dir = "amazon/";
my $amazon_summary = $data_dir . "amazon-summary.txt";
my $help = 0;
my $force_redo = 0;
my $verbose = 0;
my $recursive = 0;
my $min = 0;
my $num_p = 3;
my $num_c = 3;
my $max = 0;
my $BUFFER_SIZE = 20;
my @category_urls = ();

use threads;
use threads::shared;
use Thread::Semaphore;

my @reviews_urls :shared;
my @reviews_list :shared;
my $finish       :shared;
my $fillCount;
my $emptyCount;
my $mutex;
my $mutexURL;
my $mutexFile;


sub get_html ($) #{{{
{
    my $url = shift      || die "Nebylo zadano URL k ziskani zdrojoveho kodu.";
    my $html = get($url) || die "Nepodarilo se ziskat kod stranky na URL: $url";
    return $html;
}#}}}
sub get_product_url ($) #{{{
{
    shift =~ /(.*\/)/;
    return $1;
}#}}}
sub get_product_code ($) #{{{
{
    shift =~ /.*\/([A-Z0-9]+)\/?/;
    return $1;
}#}}}
sub get_category_url ($) #{{{
{
    shift =~ /(.*\/).*(node=\d+)/;
    return "$1?$2";
}#}}}
sub prepare_html ($) #{{{
{
    my $_ = shift;
    s/\s+/ /g;
    s/(^\s+)//;
    s/<head>.*?<\/head>//g;
    s/<style.*?<\/style>//g;
    s/<script.*?<\/script>//g;
    s/<br ?\/>/ /g;
    return $_;
}#}}}
sub get_fileName ($)#{{{
{
    my $productCode = shift;
    $data_dir.$productCode.".txt";
}#}}}
sub get_meta ($$$)#{{{
{
    my ($productCode, $title, $category) = @_;
    my $fileName = get_fileName $productCode;
    my (@meta, @categories) = ();

    push @meta, "NAME $title\n";

    if (-e $fileName)
    {
        open FILE, "<", $fileName;
        for (grep /^CATEGORY/, <FILE>)
        {
            chomp;
            push @categories, (split /\s/, $_, 2)[-1];
        }
        close FILE;
    }

    push @categories, $category unless grep /^$category$/, @categories;
    push @meta, "CATEGORY $_\n" for (@categories);

    return @meta;
}#}}}
sub get_reviews ($) #{{{
{
    my @html = split /<table id="productReviews"/, shift;
    shift @html;
    my @reviews = split /<!-- BOUNDARY -->/, shift @html;
    shift @reviews;
    return @reviews;
}#}}}
sub process_review ($) #{{{
{
    my $html = shift;
    my ($stars) = $html =~ m!<span>([0-9.]{3}) out of 5 stars</span>!;
    #$html =~ s/<br ?\/>/ /g;
    #my ($text) = $html =~ m{<span class="h3color tiny">.*?</span>.*?</b>\s?</div>(.*?)<div style="};
    my ($text) = $html =~ m{</div>\s?(.*?)<div style="[^"]+">\s?<div style="[^"]+">\s?<div style="[^"]+"><b class="tiny" style="[^"]+">Help other customers find the most helpful reviews</b>};
    $text = "text recenze nenalezen" unless $text;
    $text =~ s/^.*<\/div>//g;
    $text =~ s/^\s+//g;
    $text =~ s/<(.*?)>//g;
    return $stars, $text;
}#}}}
sub process_reviews ($$$$$) #{{{
{
    use integer;
    my ($meta, $productCode, $productName, $url, $count) = @_;
    my $fileName = get_fileName $productCode;
    my $pages = ($count % 10) ? ($count / 10) + 1 : ($count / 10);

    if ($productName)
    {
        my @stored_reviews = ();
        if (-e $fileName && !$force_redo)
        {
            # nehledat recenze pokud je stejny pocet v souboru -> pocet = 0
            open FILE, "<", $fileName or die $!;
            @stored_reviews = grep /^\d/, <FILE>;
            $count -= scalar @stored_reviews;
            close FILE;
            @stored_reviews = () if $count != 0;
        }
        else
        {
            print "Novy produkt\n" if $verbose;
        }

        print "Zpracovani produktu: $productName\n" if $verbose;
        open FILE, ">", $fileName or die "Nelze otevrit soubor ($fileName) s recenzemi: $!\n";
        print FILE @$meta;
        if (@stored_reviews)
        {
            print FILE @stored_reviews;
        }
        else
        {
            for (my $page = $pages; $page > 0; $page--)
            {
                #print "." if $verbose;
                #my $pg = $pages + 1 - $page;

                my $html = get($url."?pageNumber=".$page) || die "Nelze nacist stranku s recenzemi: $!\n";
                my @reviews = reverse get_reviews prepare_html ($html);
                while (my $review = shift @reviews)
                {
                    my ($stars, $text) = process_review $review;
                    print FILE $stars, " ", $text, "\n";
                }
                #print "\n" if $page == 1 && $verbose;
            }
        }
        close FILE;
        print "$count novych recenzi zapsano do souboru: $fileName\n\n" if $verbose;
    }
    else
    {
        warn "Nenalezen nazev produktu.\n";
    }
}#}}}
sub process_product ($) #{{{
{
    my $url = shift; #|| "http://www.amazon.com/gp/product/B0007CNY4K/ref=s9_simh_gw_p14_d25_i5?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=center-2&pf_rd_r=0YZ2YPN8BACN0FYRGKJM&pf_rd_t=101&pf_rd_p=470938631&pf_rd_i=507846";
    $url = get_product_url $url;
    my $productCode = get_product_code $url;

    my $html = get($url) or die "Terrible terrible damage with product processing. $!\n";
    #$html = prepare_html $html;
    $html =~ s/\s+/ /g;
    $html =~ s/(^\s+)//;
    $html =~ s/<style.*?<\/style>//g;
    $html =~ s/<script.*?<\/script>//g;
    $html =~ s/<br ?\/>/ /g;


    # najit odkaz na recenze, nazev produktu a pocet recenzi
    #my ($reviews, $title, $count) = $html =~ /\(<a #href="(http:\/\/www\.amazon\.com\/([A-Za-z0-9-]+)\/product-reviews\/$productCode\/).*?" >(\d+) customer reviews?<\/a>\)/;
    my ($reviews, $title, $count) = $html =~ m{\(<a href="(http://www\.amazon\.com/([A-Za-z0-9-]+)/product-reviews/$productCode).*?" >(\d+) customer reviews?</a>\)};
    if ($html =~ m{There are no customer reviews yet\.})
    {
        $reviews = "";
        $count = 0;
        ($title) = $html =~ m{href="http://www\.amazon\.com/([A-Za-z0-9-]+)/dp/$productCode};
    }

    my $category = "Neznama";
    if ($count >= $min)
    {
        my @meta = get_meta $productCode, $title, $category;
        process_reviews \@meta, $productCode, $title, $reviews, $count;
    }
    else
    {
        print "$title nema dostatecny pocet recenzi.\n\n" if $verbose;
    }
}#}}}
sub create_summary ($$$) #{{{
{
    my ($new, $category, $summary) = @_;
    open S, $new ? ">" : ">>", $amazon_summary or die $!;
    print S $category, "\n" if $new;
    print S @$summary;
    close S;
}#}}}
sub process_category #{{{
{
    my ($url, $type, $rec, $cat) = @_;

    # kontrola zacykleni
    die "Zacykleni na URL: $url\n" if scalar grep $_ eq $url, @category_urls;
    push @category_urls, $url;

    my $html = get_html $url;
    $html = prepare_html $html;

    # najit kategorii
    my ($category) = $html =~ m{<h1 id="breadCrumb">(.*?)</h1>};
    $category =~ s#</?a[^>]*>##g;
    $category =~ s#<span>.*?</span>#>#g;
    unless ($category)
    {
        $category = "Unknown";
    }

    # najit produkty
    my @products = undef;
    if ($type eq 1)
    {
        @products = $html =~ m{<div class="result[^"]+" id="result_\d+" name="[^"]+">(.*?)</div> </div> <br clear="all" />}g;
    }
    elsif ($type  eq 2)
    {
        @products = $html =~ m{<div class="result[^"]+" id="result_\d+" name="[^"]+">(.*?)</div> </div>}g;
    }
    unless (@products)
    {
        @products = $html =~ m{<table class="n2" border="0" cellpadding="0" cellspacing="0">(.*?)</table>}g;
    }

    my @summary = ();
    for (@products)
    {
        # najit odkaz na recenze, nazev produktu, kod a pocet recenzi
        #my ($reviews, $title, $productCode, $count) = m{\(<a href="(http://www\.amazon\.com/([A-Za-z0-9-]+)/product-reviews/([^"]+))" >(\d+)</a>\)};
        my ($reviews, $title, $productCode, $count) = m{\(<a href="(http://www\.amazon\.com/([^/]+)/product-reviews/([^"]+))" >([0-9,]+)</a>\)};
        unless ($reviews)
        {
            ($reviews, $productCode, $count) = m{\(<a href="(http://www\.amazon\.com/product-reviews/([^"]+))" >([0-9,]+)</a>\)};
            $title = "Neznamy";
        }
        unless ($reviews)
        {
            $reviews = "";
            $count = 0;
            ($title, $productCode) = m{<a href="http://www\.amazon\.com/([^/]+)/dp/([^"]+)"> ?<img};
            #($title, $productCode) = m{<a href="http://www\.amazon\.com/([A-Za-z0-9-]+)/dp/([^"]+)"><img};

            unless ($title && $productCode)
            {
                $title = "Neznamy";
                ($productCode) = m{<a href="http://www\.amazon\.com/dp/([^"]+)"> ?<img};
                ($productCode) = m{<div id="srProductTitle_([^_]+)_\d+" class="productTitle">} unless $productCode;
            }
        }

        # odstraneni radove carky
        $count =~ s/,//g;
        # zpracovat jen pokud vyhovuje minimu
        if ($count >= $min)
        {
            push @summary, $productCode."\t".$count."\t".$title."\n";
            my @meta = get_meta $productCode, $title, $category;
            process_reviews \@meta, $productCode, $title, $reviews, $count;
        }
        else
        {
            print "$title nema dostatecny pocet recenzi.\n\n" if $verbose;
        }
    }

    create_summary $cat, $category, \@summary;

    if ($rec)
    {
        # najit odkaz na dalsi stranku
        #<a href="http://www.amazon.com/s?ie=UTF8&rh=n%3A16318401&page=2"  class="pagnNext" id="pagnNextLink" title="Next page">Next »</a>
        my ($next) = $html =~ m{<a href="(http://www\.amazon\.com/[^"]+)"\s+class="pagnNext"\s+id="pagnNextLink"\s+title="Next page">};
        #print "NEXT: $next\n" if $next;
        process_category($next, $type, $rec, !$rec) if $next;
    }
}#}}}
sub check_dirs#{{{
{
    my $msg;
    $msg .= "Adresar $data_dir neexistuje.\n" unless -e $data_dir;
    $msg .= "$data_dir neni adresarem.\n" unless -d $data_dir;
    $msg .= "Do adresare $data_dir neni mozne zapisovat.\n" unless -w $data_dir;
    die $msg if $msg;
}#}}}
sub check_url ($)#{{{
{
    my $url = shift;
    my $_ = $url =~ m{^https?://(?:www\.)?amazon\.com/};
    if ($_)
    {
        my $html = get($url);
        # stranka s produktem
        #if ($html =~ m{<h2>Product Details</h2>})
        if ($html =~ m{<h1 class="parseasinTitle">})
        {
            $_ = 1;
        }
        # stranka kategorie
        elsif ($html =~ m{<div id="resultCount" class="resultCount">Showing ([0-9,]+) - ([0-9,]+) of [0-9,]+ Results</div>})
        {
            my ($one, $two) = ($1, $2);
            # odstraneni radove carky
            $one =~ s/,//g;
            $two =~ s/,//g;
            my $diff = $two - $one;
            # list
            if ($diff == 11)
            {
                $_ = 2;
            }
            # grid
            elsif ($diff == 23)
            {
                $_ = 3;
            }
            else
            {
                $_ = -1;
            }
        }
        else
        {
            $_ = -1;
        }
    }
    $_;
}#}}}
sub oldHelp#{{{
{
    print <<HELP;
Pouziti: $0 [options] amazon-url

Kde [options] mohou byt:
  -h    zobrazeni teto napovedy
  -d    slozka k ukladani souboru se zakaznickymi recenzemi
  -f    zpracovat znovu vsechny recenze
  -r    prochazeni cele kategorie produktu
  -v    zapnuti vypisu
  -min  stanoveni minimalniho poctu recenzi nutnych ke zpracovani

Vychozi nastaveni:
  -d    $data_dir
  -f    $force_redo
  -r    $recursive
  -v    $verbose
  -min  $min

Prilkad: $0 -v -d data/ http://www.amazon.com/gp/product/B003L785VK/

Zakaznicke recenze jsou ukladany do textovych souboru jejichz nazev je shodny
s amazonim kodem produktu (ASIN). Na zacatek souboru je umisten nazev produktu
a kategorie, do ktere produkt patri. Tyto radky jsou uvozeny klicovymi slovy
NAME a CATEGORY. Dale se na kazdem radku souboru se nachazi pocet hvezdicek
a text recenze.

Pri zpracovani kategorie je zaroven vytvoren soubor amazon_summary, ve kterem
se nachazi informace o zpracovane kategorii. Na prvnim radku se nachazi nazev
kategorie, dale je v souboru na kazdem radku nasledujici trojice hodnot:
kod-produktu    pocet-recenzi   nazev-produktu

HELP
    exit;
}#}}}
sub help#{{{
{
    print <<HELP;
Pouziti: $0 [options] amazon-url

Kde [options] mohou byt:
  -h    zobrazeni teto napovedy
  -d    slozka k ukladani souboru se zakaznickymi recenzemi
  -v    zapnuti vypisu
  -c    pocet vlaken typu konzument
  -p    pocet vlaken typu producent

Vychozi nastaveni:
  -d    $data_dir
  -v    $verbose
  -c    $num_c
  -p    $num_p

Prilkad: $0 -v -d data/ -c 3 -p 3 http://www.amazon.com/gp/product/B003L785VK/

Zakaznicke recenze jsou ulozeny do textoveho souboru jehoz nazev je shodny
s amazonim kodem produktu (ASIN). Na zacatek souboru je umisten nazev produktu
a kategorie, do ktere produkt patri. Tyto radky jsou uvozeny klicovymi slovy
NAME a CATEGORY. Dale se na kazdem radku souboru se nachazi pocet hvezdicek
a text recenze.

HELP
    exit;
}#}}}
sub save_html ($$)#{{{
{
    my $_ = shift;
    open FILE, ">", "foo.htm" or die $!;
    print FILE $_, "\n";
    my $html = get($_);
    $html = prepare_html $html if shift;
    print FILE $html;
    close FILE;
}#}}}
sub findProducts#{{{
{
    my $html = shift;
    my $type = shift;
    my @products = ();
    if ($type eq 1)
    {
        @products = $html =~ m{<div class="result[^"]+" id="result_\d+" name="[^"]+">(.*?)</div> </div> <br clear="all" />}g;
    }
    elsif ($type  eq 2)
    {
        @products = $html =~ m{<div class="result[^"]+" id="result_\d+" name="[^"]+">(.*?)</div> </div>}g;
    }
    unless (@products)
    {
        @products = $html =~ m{<table class="n2" border="0" cellpadding="0" cellspacing="0">(.*?)</table>}g;
    }

    return @products;
}#}}}
sub findProductStuff#{{{
{
    # najit odkaz na recenze, nazev produktu, kod a pocet recenzi
    #my ($reviews, $title, $productCode, $count) = m{\(<a href="(http://www\.amazon\.com/([A-Za-z0-9-]+)/product-reviews/([^"]+))" >(\d+)</a>\)};
    my ($reviews, $title, $productCode, $count) = m{\(<a href="(http://www\.amazon\.com/([^/]+)/product-reviews/([^"]+))" >([0-9,]+)</a>\)};
    unless ($reviews)
    {
        ($reviews, $productCode, $count) = m{\(<a href="(http://www\.amazon\.com/product-reviews/([^"]+))" >([0-9,]+)</a>\)};
        $title = "Neznamy";
    }
    unless ($reviews)
    {
        $reviews = "";
        $count = 0;
        ($title, $productCode) = m{<a href="http://www\.amazon\.com/([^/]+)/dp/([^"]+)"> ?<img};
        #($title, $productCode) = m{<a href="http://www\.amazon\.com/([A-Za-z0-9-]+)/dp/([^"]+)"><img};

        unless ($title && $productCode)
        {
            $title = "Neznamy";
            ($productCode) = m{<a href="http://www\.amazon\.com/dp/([^"]+)"> ?<img};
            ($productCode) = m{<div id="srProductTitle_([^_]+)_\d+" class="productTitle">} unless $productCode;
        }
    }

    # odstraneni radove carky
    $count =~ s/,//g;

    return ($reviews, $title, $productCode, $count);
}#}}}
sub findNext#{{{
{
    my $html = shift;
    my ($next) = $html =~ m{<a href="(http://www\.amazon\.com/[^"]+)"\s+class="pagnNext"\s+id="pagnNextLink"\s+title="Next page">};

    return $next ? $next : '';
}#}}}
#----------------------------------------------------------------------------
sub prepareReviewsPages#{{{
{
    print "Running prepareReviewsPages\n";
    my $url = shift || "http://www.amazon.com/gp/product/B0007CNY4K/ref=s9_simh_gw_p14_d25_i5?pf_rd_m=ATVPDKIKX0DER&pf_rd_s=center-2&pf_rd_r=0YZ2YPN8BACN0FYRGKJM&pf_rd_t=101&pf_rd_p=470938631&pf_rd_i=507846";
    $url = get_product_url $url;
    my $productCode = get_product_code $url;

    my $html = get_html $url;
    $html =~ s/\s+/ /g;
    $html =~ s/(^\s+)//;
    $html =~ s/<style.*?<\/style>//g;
    $html =~ s/<script.*?<\/script>//g;
    $html =~ s/<br ?\/>/ /g;

    my ($reviews, $title, $count) = $html =~ m{\(<a href="(http://www\.amazon\.com/([A-Za-z0-9-]+)/product-reviews/$productCode).*?" >([0-9,]+) customer reviews?</a>\)};
    if ($html =~ m{There are no customer reviews yet\.})
    {
        $reviews = "";
        $count = 0;
        ($title) = $html =~ m{href="http://www\.amazon\.com/([A-Za-z0-9-]+)/dp/$productCode};
    }
    $count =~ s/,//g;

    my $category = "Neznama";
    use integer;
    my $pages = $count / 10 + !! $count % 5;
    no integer;

    for (my $page = $pages; $page > 0; $page--)
    {
        $mutex->down;
        push @reviews_urls, "$reviews?pageNumber=$page";
        $mutex->up;
    }


    open F, ">", "$productCode.txt" || die "Problem s otevrenim souboru: $!";
    print F "NAME       $title\n";
    print F "CATEGORY   $category\n";
    close F;

    print "Prepare finished\n";
    return ($title, $count, $category, $productCode);
}#}}}
sub producent#{{{
{
    while (1)
    {
        print threads->self->tid, " mutexURL down\n" if $verbose;
        $mutexURL->down;
        my $url = shift @reviews_urls;
        $mutexURL->up;
        print threads->self->tid, " mutexURL up\n" if $verbose;

        # najit recenze
        my $html = get($url) || die "Nelze nacist stranku s recenzemi: $!\n";
        my @reviews = reverse get_reviews prepare_html ($html);
        # pro kazdou recenzi
        # ulozit hodnoceni a recenzi do sdileneho pole
        while (my $review = shift @reviews)
        {
            my ($stars, $text) = process_review $review;

            print threads->self->tid, " emptyCount down\n" if $verbose;
            $emptyCount->down;
            print threads->self->tid, " mutex down\n" if $verbose;
            $mutex->down;
            print threads->self->tid, " pushing...\n" if $verbose;
            push @reviews_list, $stars, $text;
            $mutex->up;
            print threads->self->tid, " mutexURL up\n" if $verbose;
            $fillCount->up;
            print threads->self->tid, " fillCount up\n" if $verbose;
        }

        last unless scalar @reviews_urls;
    }

    return 1;
}#}}}
sub consument#{{{
{
    my $file = shift;
    while (1)
    {
        print threads->self->tid, " fillCount down\n" if $verbose;
        $fillCount->down;
        unless ($finish && not scalar @reviews_list)
        {
            print threads->self->tid, " mutex down\n" if $verbose;
            $mutex->down;
            my $stars = shift @reviews_list;
            my $review = shift @reviews_list;
            $mutex->up;
            print threads->self->tid, " mutex up\n" if $verbose;
            $emptyCount->up;
            print threads->self->tid, " emptyCount up\n" if $verbose;

            print threads->self->tid, " mutexFile down\n" if $verbose;
            $mutexFile->down;
            open F, ">>", "$file.txt" || die "Problem s otevrenim souboru: $!";
            print F join("\t", $stars, $review), "\n";
            close F;
            $mutexFile->up;
            print threads->self->tid, " mutexFile up\n" if $verbose;
        }

        last if ($finish && not scalar @reviews_list);
    }
}#}}}
sub ppr#{{{
{
    $|++;
    my $url = shift;
    my $num_p = shift;
    my $num_c = shift;
    print "Paralelni zpracovani se $num_p producenty a $num_c konzumenty.\n";
    my ($productName, $count, $category, $code) = prepareReviewsPages($url, $max);

    my @consuments = ();
    my @producents = ();
    push @consuments, threads->new(\&consument, $code)  for 1..$num_c;
    push @producents, threads->new(\&producent)         for 1..$num_p;

    if (eval join(" && ", map { $_->join } @producents))
    {
        $finish = 1;
        #print "RWL:  ", scalar @reviews_list, "\n";
        #print "URLS: ", scalar @reviews_urls, "\n";
        #print "snad tam nikdo nezustal\n";
        $fillCount->up(scalar @consuments);
        print "Prace producentu dokoncena\n";
    }
    $_->join for @consuments;
    print "Prace konzumentu dokoncena\n";
}#}}}
#----------------------------------------------------------------------------
# MAIN {{{

my $status = GetOptions(
    "h" => \$help,
    "f" => \$force_redo,
    "d:s" => \$data_dir,
    "v" => \$verbose,
    "p:i" => \$num_p,
    "c:i" => \$num_c,
    "b:i" => \$BUFFER_SIZE,
);


help if (!$status || $help );#|| !@ARGV);

$data_dir .= "/" unless $data_dir =~ m{/$};

check_dirs;

$amazon_summary = $data_dir . "amazon-summary.txt";

@reviews_urls = ();
@reviews_list = ();
$finish       = 0;
$fillCount    = Thread::Semaphore->new(0);
$emptyCount   = Thread::Semaphore->new($BUFFER_SIZE);
$mutex        = Thread::Semaphore->new(1);
$mutexURL     = Thread::Semaphore->new(1);
$mutexFile    = Thread::Semaphore->new(1);

#use Dumpvalue;
#my $dump = new Dumpvalue;
#$dump->dumpValue(\$emptyCount);

my @urls = @ARGV;
for my $url (@urls)
{
    my $check = check_url $url;
    if ($check == 0)
    {
        warn "URL nema spravny tvar.\n";
    }
    elsif ($check == 1)
    {
        ppr $url, $num_p, $num_c;
    }
    else
    {
        warn "URL neodkazuje na stranku s produktem.";
    }
}

# }}}

my $end = time;
printf("%s%.2f%s", "\nCas zpracovani: ", $end - $start, "s");

print "\n"
# vim:fdm=marker:nowrap
